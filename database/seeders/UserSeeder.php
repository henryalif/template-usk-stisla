<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Administrator",
            "email" => "admin@admin.com",
            "email_verified_at" => now(),
            "password" => Hash::make("admin@admin.com"),
            "role_id" => 1
        ]);

        User::create([
            "name" => "Bank",
            "email" => "bank@bank.com",
            "email_verified_at" => now(),
            "password" => Hash::make("bank@bank.com"),
            "role_id" => 2
        ]);

        User::create([
            "name" => "Shop",
            "email" => "shop@shop.com",
            "email_verified_at" => now(),
            "password" => Hash::make("shop@shop.com"),
            "role_id" => 3
        ]);

        User::create([
            "name" => "Student",
            "email" => "student@student.com",
            "email_verified_at" => now(),
            "password" => Hash::make("student@student.com"),
            "role_id" => 4
        ]);
    }
}
