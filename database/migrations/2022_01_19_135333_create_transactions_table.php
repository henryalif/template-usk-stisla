<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('product_id')->nullable()->constrained();
            $table->integer('qty');
            $table->string('invoice_id')->nullable();
            $table->integer('type');
            $table->integer('status');
            $table->timestamps();
        });
    }

    // Status
    // 1 = On cart
    // 2 = On Checkout
    // 3 = Pay
    // 4 = Accept
    // 5 = Rejected
    // 0 = Finish

    // Type
    // 1 = Top Up Balance
    // 2 = Shopping

    // Invoice =
    // TOP_ = For Top Up Balance
    // INV_ = For Shopping

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
