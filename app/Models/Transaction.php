<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'product_id',
        'invoice_id',
        'qty',
        'type',
        'status'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
    // Status
    // 1 = On cart
    // 2 = On Checkout
    // 3 = Pay
    // 4 = Accept
    // 5 = Rejected
    // 0 = Finish

    // Type
    // 1 = Top Up Balance
    // 2 = Shopping

    // Invoice =
    // TOP_ = For Top Up Balance
    // INV_ = For Shopping
