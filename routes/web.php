<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', [HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/blank', function () {
    return view('blank');
});

Route::prefix('bank')->group(function () {
    Route::get('/', function () {
        return view('bank.index');
    });

    Route::get('/admin', function () {
        return view('bank.admin');
    });
});

Route::prefix('product')->group(function () {
    Route::get('/', function () {
        return view('product.index');
    });

    Route::get('/admin', function () {
        return view('product.admin');
    });

    Route::get('/approval', function () {
        return view('product.approval');
    });

    Route::get('/checkout', function () {
        return view('product.checkout');
    });
});

Route::get('/history', function () {
    return view('history.index');
});

Route::prefix("/user")->group( function() {
    Route::get('/', function(){
        return view('user');
    });
    // Route::post('/register', [RegisterController::class, 'create'])->name('register');
});

// Route::get('/user', function () {
//     return view('user');
// });
