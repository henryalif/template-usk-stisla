<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/home">
        <i class=" fas fa-home"></i><span>Dashboard</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/product">
        <i class=" fas fa-utensils"></i><span>Product</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/product/admin">
        <i class=" fas fa-truck-loading"></i><span>Product Admin</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/product/checkout">
        <i class=" fas fa-check"></i><span>Checkout</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/bank">
        <i class=" fas fa-building"></i><span>Bank</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/bank/admin">
        <i class=" fas fa-users-cog"></i><span>Bank Admin</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/product/approval">
        <i class=" fas fa-store"></i><span>Canteen Approval</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/user">
        <i class=" fas fa-users"></i><span>Add User</span>
    </a>
</li>

<li class="side-menus {{ Request::is('*') ? 'active' : '' }}">
    <a class="nav-link" href="/history">
        <i class=" fas fa-history"></i><span>History</span>
    </a>
</li>
