@extends('layouts.app')


@section('content')
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-responsive-xl">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Menu</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Nasi</td>
                                <td>1</td>
                                <td><span class="badge badge-success"><strong>Rp. 90.000</strong></span></td>
                                <td><span class="badge badge-primary"><strong>Rp. 90.000</strong></span></td>
                            </tr>
                        </tbody>
                    </table>
                    Total Transaction :
                    <span class="badge badge-dark"><Strong>Rp. 90.000</Strong></span>
                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Canteen Approval</h3>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Invoice</th>
                                    <th>Status</th>
                                    <th>Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td>1</td>
                                    <td><span class="badge badge-dark">Henry</div></td>
                                    <td><span class="badge badge-info"><strong>#INV_8293293098</strong></span></td>
                                    <td>Completed</td>
                                    <td>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Detail
                                        </button>
                                    </td>
                                    <td>
                                        <a class="btn btn-success btn-md" href="#" role="button"><i class="fas fa-check-square"></i></a>
                                        <a class="btn btn-danger btn-md" href="#" role="button"><i class="fas fa-times-circle"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>

@endsection
