@extends('layouts.app')

@section('content')
{{-- ,odal --}}
<div class="modal fade" id="cardModal" tabindex="-1" aria-labelledby="cardModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="cardModalLabel">Your Cart</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <table class="table">
                <thead>
                  <tr>
                      <th>#</th>
                      <th>Item</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Total</th>
                      <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>#</td>
                        <td><span class="badge badge-info">Pizza</span></td>
                        <td>2</td>
                        <td><Strong>Rp. 50.000</Strong></td>
                        <td><span class="badge badge-info"><Strong>Rp. 100.000</Strong></span></td>
                        <td>
                            <a class="btn btn-danger btn-md" href="#" role="button"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="mt-3 mb-3">Total transaction : <strong>Rp. 100.000</strong></p>
            {{-- <h6><strong>Rp. 100.000</strong></h6> --}}
            <a href="#" class="btn btn-success btn-block mt-3 mb-3">Checkout!</a>
        </div>
    </div>
    </div>
</div>

    <section class="section">
        <div class="section-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                      <h5 class="page__heading">Product Page</h5>
                    </div>
                    <div class="col text-right">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#cardModal">Cart</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header mb-0">
                                    <h5 class="card-title">Product Name</h5>
                                </div>
                                <div class="card-body">
                                    <p class="card-text mb-0">Product description.</p>
                                    <h6 class="card-text mb-3">Rp. 8.000</h6>
                                    {{-- <form method="POST" action="#">
                                        <input type="number" name="jumlah" class="form-control" value="1">
                                        <input type="hidden" name="barang_id" value="#">
                                        <button class="btn btn-primary" type="submit"><i class="fas fa-plus-circle"></i></button>
                                    </form> --}}

                                <form action="" method="POST">
                                    @csrf
                                    <div class="input-group mb-3">
                                        <input type="number" class="form-control" name="qty" placeholder="Item" aria-label="Item" aria-describedby="button-addon2" value="1">
                                        <input type="hidden" name="product_id" value="#">
                                        <button class="btn btn-outline-success" type="submit" id="button-addon2"><i class="fas fa-cart-plus"></i></button>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable({
    "scrollY": "50vh",
    "scrollCollapse": true,
    });
    } );
</script>

@endsection
